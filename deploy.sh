#! /bin/bash
git clone --branch 8.x http://git.drupal.org/project/drupal.git drupal
cd drupal/core
./vendor/bin/phpunit --coverage-html ../../coverage
