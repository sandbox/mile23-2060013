WHAT IS IT?
===========

Drupalcoverage generates a PHPUnit coverage report for Drupal 8, deployed to Pagodabox.

You can see it here: http://drupalcoverage.gopagoda.com/ It will be periodically updated when I re-deploy. You can see the deployment date on the bottom right corner.

It's a seriously tiny hack. :-)

Want to see some worrisome stuff? Look at the dashboard, like this: http://drupalcoverage.gopagoda.com/var_www_drupal_core_lib_Drupal.dashboard.html

Anyone is welcome to deploy this. Just get a Pagodabox account and push this repo to it. If you end up giving it the ability to deploy to another service, please file an issue with a patch and we'll add it.
